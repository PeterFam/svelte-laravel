import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import purgecss from '@fullhuman/postcss-purgecss'
import PurgeSvelte from 'purgecss-from-svelte'

const options = {
  content: ["./src/**/*.svelte", './index.html'],
  safelist: [/svelte-/],
  extractors: [
    {
      extractor: content => PurgeSvelte.extract(content),
      extensions: ["svelte"]
    }
  ]
};

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => ({
  plugins: [svelte()],
  css: {
    postcss: {
      plugins: mode === 'production' ? [purgecss(options)] : []
    }
  }
}))
